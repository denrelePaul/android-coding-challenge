package com.example.binlist.Model

import com.google.gson.annotations.SerializedName


data class Number(val length: Int, val luhn: Boolean)
    data class brand(val brand: String)
    data class Scheme(val scheme: String)
    data class Type(val type: String)
    data class Prepaid(val prepaid: Boolean)
    data class Country(
        val name:String,
        val emoji: emoji,
        val latitude: latitude,
        val longitude: longitude
    )



data class emoji (val emoji:String)
data class latitude(val latitude:String)
data class longitude (val longitude:String)



    data class Bank(val name: String, val url: String, val phone: String, val city: String)

