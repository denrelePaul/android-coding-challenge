package com.example.binlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.binlist.Model.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_edit_acitivity.*
import kotlinx.android.synthetic.main.activity_edit_acitivity.txt_search_brand
import kotlinx.android.synthetic.main.activity_edit_acitivity.txt_search_emoji
import kotlinx.android.synthetic.main.activity_edit_acitivity.txt_search_name
import kotlinx.android.synthetic.main.activity_edit_acitivity.txt_search_prepaid
import kotlinx.android.synthetic.main.activity_edit_acitivity.txt_search_scheme
import kotlinx.android.synthetic.main.activity_edit_acitivity.txt_search_type
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditAcitivity : AppCompatActivity() {


    private var disposable: Disposable? = null
    val apiInterface: ApiInterface? = null
    private var myCompositeDisposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_acitivity)




        btn_search_edt.setOnClickListener {v ->

            if (edit_search.text.toString().isNotEmpty()) {
                loadData(edit_search.text.toString())
            }

        }


    }

    private val ApiServe by lazy {
        ApiInterface.create()
    }



    private fun loadData(searchString: String) {
        ApiServe.getScheme("https://lookup.binlist.net/${searchString}").enqueue(object :
            Callback<Scheme> {
            override fun onFailure(call: Call<Scheme>, t: Throwable) {
                Toast.makeText(this@EditAcitivity , "Failed" , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Scheme>, response: Response<Scheme>) {
                val sub = response.body()
                txt_search_scheme.text = "${sub!!.scheme}"
                Toast.makeText(this@EditAcitivity , " response ${sub.scheme}" , Toast.LENGTH_LONG).show()

            }

        })



        ApiServe.getBrand("https://lookup.binlist.net/${searchString}").enqueue(object :
            Callback<brand> {
            override fun onFailure(call: Call<brand>, t: Throwable) {
                Toast.makeText(this@EditAcitivity , "Failed" , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<brand>, response: Response<brand>) {
                val sub = response.body()
                txt_search_brand.text = "${sub!!.brand}"
                Toast.makeText(this@EditAcitivity , " brand ${sub.brand}" , Toast.LENGTH_LONG).show()

            }

        })



        /*  ApiServe.getNumber("https://lookup.binlist.net/${searchString}").enqueue(object :Callback<Number>{
               override fun onFailure(call: Call<com.example.binlist.Model.Number>, t: Throwable) {
                   Toast.makeText(this@MainActivity , "Failed" , Toast.LENGTH_LONG).show()
               }

               override fun onResponse(call: Call<com.example.binlist.Model.Number>, response: Response<com.example.binlist.Model.Number>) {
                   val sub = response.body()
                   txt_search_number.text = "${sub!!.length}"
                   txt_search_luhn.text = "${sub.luhn}"
                   Toast.makeText(this@MainActivity , "numbs ${response.body()}" , Toast.LENGTH_LONG).show()

               }

           })
   */

        ApiServe.getCountry("https://lookup.binlist.net/${searchString}").enqueue(object :
            Callback<Country> {
            override fun onFailure(call: Call<Country>, t: Throwable) {
                Toast.makeText(this@EditAcitivity , "Failed" , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Country>, response: Response<Country>) {
                val sub = response.body()
                txt_search_emoji.text = "${sub!!.emoji}"
                txt_search_name.text = "${sub.name}"
                //   txt_search_latitude.text = "${sub!!.latitude}"
                //    txt_search_longitude.text = "${sub!!.longitude}"

                Toast.makeText(this@EditAcitivity , "Country ${sub}" , Toast.LENGTH_LONG).show()

            }

        })



        ApiServe.getType("https://lookup.binlist.net/${searchString}").enqueue(object :
            Callback<Type> {
            override fun onFailure(call: Call<Type>, t: Throwable) {
                Toast.makeText(this@EditAcitivity , "Failed" , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Type>, response: Response<Type>) {
                val sub = response.body()
                txt_search_type.text = "${sub!!.type}"

                Toast.makeText(this@EditAcitivity , "Type ${sub}" , Toast.LENGTH_LONG).show()

            }

        })





        ApiServe.getPrepaid("https://lookup.binlist.net/${searchString}").enqueue(object :
            Callback<Prepaid> {
            override fun onFailure(call: Call<Prepaid>, t: Throwable) {
                Toast.makeText(this@EditAcitivity , "Failed" , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Prepaid>, response: Response<Prepaid>) {
                val sub = response.body()
                txt_search_prepaid.text = "${sub!!.prepaid}"

                Toast.makeText(this@EditAcitivity , "Country ${sub}" , Toast.LENGTH_LONG).show()

            }

        })



    }

}
