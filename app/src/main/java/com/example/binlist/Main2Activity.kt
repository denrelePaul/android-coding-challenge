package com.example.binlist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.binlist.Model.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_edit_acitivity.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.txt_search_brand
import kotlinx.android.synthetic.main.activity_main.txt_search_emoji
import kotlinx.android.synthetic.main.activity_main.txt_search_name
import kotlinx.android.synthetic.main.activity_main.txt_search_prepaid
import kotlinx.android.synthetic.main.activity_main.txt_search_scheme
import kotlinx.android.synthetic.main.activity_main.txt_search_type
import kotlinx.android.synthetic.main.activity_main2.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Main2Activity : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)



        scan.setOnClickListener { v ->

            val intent = Intent(this@Main2Activity, MainActivity::class.java)
            startActivity(intent)

        }


        edit.setOnClickListener { v ->
            val intent = Intent(this@Main2Activity, EditAcitivity::class.java)
            startActivity(intent)

        }
    }




}
