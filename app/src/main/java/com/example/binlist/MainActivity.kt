package com.example.binlist

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.SurfaceHolder
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.example.binlist.Model.*
import com.example.binlist.Model.Number
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.text.TextBlock
import com.google.android.gms.vision.text.TextRecognizer
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url
import java.lang.StringBuilder
import java.util.*
import kotlin.properties.Delegates


class MainActivity : AppCompatActivity() {


    private var disposable: Disposable? = null
     val apiInterface: ApiInterface? = null
    private var myCompositeDisposable: CompositeDisposable? = null


    private var mCameraSource by Delegates.notNull<CameraSource>()
    private var textRecognizer by Delegates.notNull<TextRecognizer>()

    //PERMISSIONS
    private val PERMISSION_REQUEST_CAMERA = 100

    //TAG
    private val tag: String? = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startCameraSource()

        btn_search.setOnClickListener {
            if (tv_result.text.toString().isNotEmpty()) {
               loadData(tv_result.text.toString())
            }
        }



    }

    private val ApiServe by lazy {
        ApiInterface.create()
    }



    private fun loadData(searchString: String) {
        ApiServe.getScheme("https://lookup.binlist.net/${searchString}").enqueue(object :Callback<Scheme>{
            override fun onFailure(call: Call<Scheme>, t: Throwable) {
                Toast.makeText(this@MainActivity , "Failed" , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Scheme>, response: Response<Scheme>) {
                  val sub = response.body()
                txt_search_scheme.text = "${sub!!.scheme}"
                Toast.makeText(this@MainActivity , " response ${sub.scheme}" , Toast.LENGTH_LONG).show()

            }

        })



        ApiServe.getBrand("https://lookup.binlist.net/${searchString}").enqueue(object :Callback<brand>{
            override fun onFailure(call: Call<brand>, t: Throwable) {
                Toast.makeText(this@MainActivity , "Failed" , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<brand>, response: Response<brand>) {
                val sub = response.body()
                txt_search_brand.text = "${sub!!.brand}"
                Toast.makeText(this@MainActivity , " brand ${sub.brand}" , Toast.LENGTH_LONG).show()

            }

        })



     /*  ApiServe.getNumber("https://lookup.binlist.net/${searchString}").enqueue(object :Callback<Number>{
            override fun onFailure(call: Call<com.example.binlist.Model.Number>, t: Throwable) {
                Toast.makeText(this@MainActivity , "Failed" , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<com.example.binlist.Model.Number>, response: Response<com.example.binlist.Model.Number>) {
                val sub = response.body()
                txt_search_number.text = "${sub!!.length}"
                txt_search_luhn.text = "${sub.luhn}"
                Toast.makeText(this@MainActivity , "numbs ${response.body()}" , Toast.LENGTH_LONG).show()

            }

        })
*/

        ApiServe.getCountry("https://lookup.binlist.net/${searchString}").enqueue(object :Callback<Country>{
            override fun onFailure(call: Call<Country>, t: Throwable) {
                Toast.makeText(this@MainActivity , "Failed" , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Country>, response: Response<Country>) {
                val sub = response.body()
                txt_search_emoji.text = "${sub!!.emoji}"
                txt_search_name.text = "${sub.name}"
             //   txt_search_latitude.text = "${sub!!.latitude}"
            //    txt_search_longitude.text = "${sub!!.longitude}"

                Toast.makeText(this@MainActivity , "Country ${sub}" , Toast.LENGTH_LONG).show()

            }

        })



        ApiServe.getType("https://lookup.binlist.net/${searchString}").enqueue(object :Callback<Type>{
            override fun onFailure(call: Call<Type>, t: Throwable) {
                Toast.makeText(this@MainActivity , "Failed" , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Type>, response: Response<Type>) {
                val sub = response.body()
                txt_search_type.text = "${sub!!.type}"

                Toast.makeText(this@MainActivity , "Type ${sub}" , Toast.LENGTH_LONG).show()

            }

        })





        ApiServe.getPrepaid("https://lookup.binlist.net/${searchString}").enqueue(object :Callback<Prepaid>{
            override fun onFailure(call: Call<Prepaid>, t: Throwable) {
                Toast.makeText(this@MainActivity , "Failed" , Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Prepaid>, response: Response<Prepaid>) {
                val sub = response.body()
                txt_search_prepaid.text = "${sub!!.prepaid}"

                Toast.makeText(this@MainActivity , "Country ${sub}" , Toast.LENGTH_LONG).show()

            }

        })



    }



    private fun startCameraSource() {

        //  Create text Recognizer
        textRecognizer = TextRecognizer.Builder(this).build()

        if (!textRecognizer.isOperational) {
            Toast.makeText(this , "Dependencies are not loaded yet...please try after few moment!!" ,Toast.LENGTH_LONG).show()
            Log.d(tag ,"Dependencies are downloading....try after few moment")
            return
        }

        //  Init camera source to use high resolution and auto focus
        mCameraSource = CameraSource.Builder(applicationContext, textRecognizer)
            .setFacing(CameraSource.CAMERA_FACING_BACK)
            .setRequestedPreviewSize(1280, 1024)
            .setAutoFocusEnabled(true)
            .setRequestedFps(2.0f)
            .build()

        surface_preview.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {

            }

            override fun surfaceDestroyed(p0: SurfaceHolder?) {
                mCameraSource.stop()
            }

            @SuppressLint("MissingPermission")
            override fun surfaceCreated(p0: SurfaceHolder?) {
                try {
                    if (isCameraPermissionGranted()) {
                        mCameraSource.start(surface_preview.holder)
                    } else {
                        requestForPermission()
                    }
                } catch (e: Exception) {
                    Toast.makeText(this@MainActivity , "Error:  ${e.message}" , Toast.LENGTH_LONG).show()
                }
            }
        })

        textRecognizer.setProcessor(object : Detector.Processor<TextBlock> {
            override fun release() {}

            override fun receiveDetections(detections: Detector.Detections<TextBlock>) {
                val items = detections.detectedItems

                if (items.size() <= 0) {
                    return
                }

                tv_result.post {
                    val stringBuilder = StringBuilder()
                    val str = String()
                    for (i in 0 until items.size()) {
                        val item = items.valueAt(i)
                        stringBuilder.append(item.value)
                        stringBuilder.append("\n")
                        str.replace("\\D+", "")
                         }
                    tv_result.text = stringBuilder.toString()



                }
            }
        })
    }

    fun isCameraPermissionGranted(): Boolean {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED
    }

    private fun requestForPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), PERMISSION_REQUEST_CAMERA)
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode != PERMISSION_REQUEST_CAMERA) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (isCameraPermissionGranted()) {
                mCameraSource.start(surface_preview.holder)
            } else {
                Toast.makeText(this , "Permission need to grant" , Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }


}











